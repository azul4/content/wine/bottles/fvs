# fvs

File Versioning System with hash comparison, deduplication and data storage to create unlinked states that can be deleted

https://github.com/mirkobrombin/FVS

<br>

How to clone this repository:
```
git clone https://gitlab.com/azul4/content/wine/bottles/fvs.git
```

